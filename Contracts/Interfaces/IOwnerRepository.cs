﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Interfaces
{
  public  interface IOwnerRepository //: IRepositoryBase<Owner>
    {
        IEnumerable<Owner> GetAllOwners();
        Owner GetOwnerById(Guid ownerId);
        Owner GetOwnerWithDetails(Guid ownerId);
        
    }
}
