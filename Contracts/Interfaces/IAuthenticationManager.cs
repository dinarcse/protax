﻿using Entities.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
  public  interface IAuthenticationManager
    {
        Task<bool> ValidateUser(UserLoginDto userForAuth);
        Task<string> CreateToken();
    }
}
