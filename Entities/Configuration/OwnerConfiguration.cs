﻿using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Configuration
{
    public class OwnerConfiguration : IEntityTypeConfiguration<Owner>
    {
        public void Configure(EntityTypeBuilder<Owner> builder)
        {
            builder.ToTable("Owner");
            //builder.Property(s => s.Age)
            //    .IsRequired(false);
            //builder.Property(s => s.IsRegularStudent)
            //    .HasDefaultValue(true);

            builder.HasData
            (
                new Owner
                {
                    Id = Guid.NewGuid(),
                    Name = "John Doe",
                    DateOfBirth = new DateTime(),
                    Address  = "Onek Dur"
                },
                new Owner
                {
                    Id = Guid.NewGuid(),
                    Name = "Jane Doe",
                    DateOfBirth = new DateTime(),
                    Address = "Khub Kache"
                },
                new Owner
                {
                    Id = Guid.NewGuid(),
                    Name = "Mike Miles",
                    DateOfBirth = new DateTime(),
                     Address = "Khub Kache o na dureo na "
                }
            );
        }

       
    }
}
