﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class FluentAPIConfigurationApproachApplied : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("abc61d8b-9766-4376-94cb-edc58902ec09"), "Onek Dur", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "John Doe" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("4a07b448-7567-4a24-a8f9-73a6934e2458"), "Khub Kache", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jane Doe" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("df47000a-75bb-4faa-a1ea-fcdd271e8c90"), "Khub Kache o na dureo na ", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mike Miles" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("4a07b448-7567-4a24-a8f9-73a6934e2458"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("abc61d8b-9766-4376-94cb-edc58902ec09"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("df47000a-75bb-4faa-a1ea-fcdd271e8c90"));
        }
    }
}
