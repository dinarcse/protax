﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class IdentityRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("5d0c6cf5-53d0-4a2a-a0f9-29d0dd469e60"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("9c105a34-93a7-488d-b919-91a112527299"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("de20e6f5-51aa-4a25-b597-d1fe8d91097f"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "2d120531-f2ba-4b1e-8606-6112133dbf64", "601d0e96-6797-4abf-8821-0670ff923d75", "Visitor", "VISITOR" },
                    { "ee93fc0c-2079-4aa0-b69a-05861b8e6240", "bcbd4033-42d9-401a-a123-baccba0debd7", "Administrator", "ADMINISTRATOR" }
                });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[,]
                {
                    { new Guid("ca5ca83c-565c-4ffb-a966-d18e952c2dd3"), "Onek Dur", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "John Doe" },
                    { new Guid("071ccbe2-0bb0-4638-b360-22236e903e53"), "Khub Kache", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jane Doe" },
                    { new Guid("65b430f1-ea5a-4aa2-8cf1-66c47756ba04"), "Khub Kache o na dureo na ", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mike Miles" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2d120531-f2ba-4b1e-8606-6112133dbf64");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ee93fc0c-2079-4aa0-b69a-05861b8e6240");

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("071ccbe2-0bb0-4638-b360-22236e903e53"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("65b430f1-ea5a-4aa2-8cf1-66c47756ba04"));

            migrationBuilder.DeleteData(
                table: "Owner",
                keyColumn: "OwnerId",
                keyValue: new Guid("ca5ca83c-565c-4ffb-a966-d18e952c2dd3"));

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("de20e6f5-51aa-4a25-b597-d1fe8d91097f"), "Onek Dur", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "John Doe" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("9c105a34-93a7-488d-b919-91a112527299"), "Khub Kache", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jane Doe" });

            migrationBuilder.InsertData(
                table: "Owner",
                columns: new[] { "OwnerId", "Address", "DateOfBirth", "Name" },
                values: new object[] { new Guid("5d0c6cf5-53d0-4a2a-a0f9-29d0dd469e60"), "Khub Kache o na dureo na ", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mike Miles" });
        }
    }
}
