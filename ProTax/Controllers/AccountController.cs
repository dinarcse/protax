﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Interfaces;
using EmailService;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProTax.ActionFilters;

namespace ProTax.Controllers
{


    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        private readonly IMapper _mapper;
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly IAuthenticationManager _authManager;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly SignInManager<User> _signInManager;

        public AccountController(IMapper mapper, UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IAuthenticationManager authManager ,
            SignInManager<User> signInManager, ILoggerManager logger, IEmailSender emailSender)
        {
            _mapper = mapper;
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _roleManager = roleManager;
            _authManager = authManager;
        }


        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> Register([FromBody] UserRegistrationDto userModel)
        {
            var user = _mapper.Map<User>(userModel);
            var result = await _userManager.CreateAsync(user, userModel.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors) { ModelState.TryAddModelError(error.Code, error.Description); }

                return BadRequest(ModelState);
            }

            if (!userModel.Roles.Any())
            {
                _logger.LogInfo("Roles doesn't exist in the registration DTO object, adding the default one.");
                await _userManager.AddToRoleAsync(user, "Visitor");
            }
            else
            {
                foreach (var roleName in userModel.Roles)
                {

                    var isRoleExist = await _roleManager.RoleExistsAsync(roleName);
                    var role = new IdentityRole(roleName);
                    if (!isRoleExist)
                    {
                        await _roleManager.CreateAsync(role);
                    }
                }

                await _userManager.AddToRolesAsync(user, userModel.Roles);
            }

            return StatusCode(201);

        }

      

        [HttpPost("login")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> Authenticate([FromBody] UserLoginDto user)
        {
            if (!await _authManager.ValidateUser(user))
            {
                _logger.LogWarn($"{nameof(Authenticate)}: Authentication failed. Wrong user name or password.");
                return Unauthorized();
            }

            return Ok(new { Token = await _authManager.CreateToken() });
        }



      
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return StatusCode(StatusCodes.Status200OK, "Logged Out");
        }

      


        

        
    }
}