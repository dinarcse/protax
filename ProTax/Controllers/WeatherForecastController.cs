﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Contracts.Interfaces;
using EmailService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ProTax.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILoggerManager _logger;
        private readonly IEmailSender _emailSender;

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherForecastController(IRepositoryWrapper repoWrapper, ILoggerManager logger, IEmailSender emailSender)
        {
            _repoWrapper = repoWrapper;
            _emailSender = emailSender;
            _logger = logger;

        }


        [HttpGet, Authorize]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();

            var message = new Message(new string[] { "shorola76@gmail.com" }, "Moja Nilam", "Moja dilam through out an email bombing");
           

                _emailSender.SendEmail(message);
            

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }


        // GET api/values
        //[HttpGet]
        //[Authorize]
        //public IEnumerable<string> Get()
        //{
        //    var domesticAccounts = _repoWrapper.Account.FindByCondition(x => x.AccountType.Equals("Domestic"));
        //    var owners = _repoWrapper.Owner.GetAllOwners();
        //    _logger.LogInfo("Here is info message from the controller DNR.");
        //    _logger.LogDebug("Here is debug message from the controller DNR.");
        //    _logger.LogWarn("Here is warn message from the controller DNR.");
        //    _logger.LogError("Here is error message from the controller DNR.");
        //    return new string[] { "value1", "value2" };
        //}
    }

    //[ApiController]
    //[Route("[controller]")]

    //public class WeatherForecastController : ControllerBase
    //{
    //    private readonly ILoggerManager _logger;

    //    public WeatherForecastController(ILoggerManager logger)
    //    {
    //        _logger = logger;
    //    }

    //    [HttpGet]
    //    public IEnumerable<string> Get()
    //    {
    //        _logger.LogInfo("Here is info message from the controller.");
    //        _logger.LogDebug("Here is debug message from the controller.");
    //        _logger.LogWarn("Here is warn message from the controller.");
    //        _logger.LogError("Here is error message from the controller.");

    //        return new string[] { "value1", "value2" };
    //    }
    //}
    //[ApiController]
    //[Route("[controller]")]
    //public class WeatherForecastController : ControllerBase
    //{
    //    private static readonly string[] Summaries = new[]
    //    {
    //        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    //    };

    //    private readonly ILogger<WeatherForecastController> _logger;

    //    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    //    {
    //        _logger = logger;
    //    }

    //    [HttpGet]
    //    public IEnumerable<WeatherForecast> Get()
    //    {
    //        var rng = new Random();
    //        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
    //        {
    //            Date = DateTime.Now.AddDays(index),
    //            TemperatureC = rng.Next(-20, 55),
    //            Summary = Summaries[rng.Next(Summaries.Length)]
    //        })
    //        .ToArray();
    //    }
    //}
}
