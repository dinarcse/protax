﻿using Contracts;
using Contracts.Interfaces;
using EmailService;
using Entities;
using Entities.Factory;
using Entities.Models;
using LoggerService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using ProTax.ActionFilters;
using ProTax.Utility;

namespace ProTax.Extensions
{
    public static class ServiceExtensions
    {


        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }


        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            //services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }


        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config["ConnectionStrings:RepositoryContext"];
            services.AddDbContextPool<RepositoryContext>(o => o.UseSqlServer(connectionString));
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }

        public static void ConfigureAuthenticationManager(this IServiceCollection services) =>
          services.AddScoped<IAuthenticationManager, AuthenticationManager>();
        public static void ConfigureActionFilters(this IServiceCollection services)
        {
           
            services.AddScoped<ValidationFilterAttribute>();
        }


        public static void ConfigureCustomClaimsFactory(this IServiceCollection services)
        {
            services.AddScoped<IUserClaimsPrincipalFactory<User>, CustomClaimsFactory>();
        }

        public static void ConfigureIdentityService(this IServiceCollection services)
        {
            // services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<RepositoryContext>();

            services.AddIdentity<User, IdentityRole>(opt =>
            {
                opt.Password.RequiredLength = 7;
                opt.Password.RequireDigit = false;
                opt.Password.RequireUppercase = false;
                opt.User.RequireUniqueEmail = true;
            }).AddEntityFrameworkStores<RepositoryContext>()
              .AddDefaultTokenProviders();

            //var builder = services.AddIdentityCore<User>(o => 
            //{ o.Password.RequireDigit = true;
            //    o.Password.RequireLowercase = false; 
            //    o.Password.RequireUppercase = false; 
            //    o.Password.RequireNonAlphanumeric = false;
            //    o.Password.RequiredLength = 10; 
            //    o.User.RequireUniqueEmail = true; });

            //builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services); 
            //builder.AddEntityFrameworkStores<RepositoryContext>().AddDefaultTokenProviders();
        }

        public static void ConfigureTokenLifeSpan(this IServiceCollection services)
        {


            services.Configure<DataProtectionTokenProviderOptions>(opt =>
               opt.TokenLifespan = TimeSpan.FromMinutes(30));
        }

    //    public static void configurejsonwebbearer(this iservicecollection services)
    //    {


    //        services.addauthentication(opt =>
    //        {
    //            opt.defaultauthenticatescheme = jwtbearerdefaults.authenticationscheme;
    //            opt.defaultchallengescheme = jwtbearerdefaults.authenticationscheme;
    //        })
    //.addjwtbearer(options =>
    //{
    //    options.tokenvalidationparameters = new tokenvalidationparameters
    //    {
    //        validateissuer = true,
    //        validateaudience = true,
    //        validatelifetime = true,
    //        validateissuersigningkey = true,

    //        validissuer = "http://localhost:5000",
    //        validaudience = "http://localhost:5000",
    //        issuersigningkey = new symmetricsecuritykey(encoding.utf8.getbytes("supersecretkey@345"))
    //    };
    //});
    //    }


        public static void ConfigureJSonWebBearer(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettings = configuration.GetSection("JwtSettings"); var secretKey = Environment.GetEnvironmentVariable("SECRET");

            services.AddAuthentication(opt =>
            { opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme; 
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme; 
            })
                .AddJwtBearer(options => {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ValidIssuer = jwtSettings.GetSection("validIssuer").Value,
                    ValidAudience = jwtSettings.GetSection("validAudience").Value,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey))
                };
            });
        }
        public static void ConfigureEmialService(this IServiceCollection services, IConfiguration config)
        {

            var emailConfig = config
         .GetSection("EmailConfiguration")
         .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IEmailSender, EmailSender>();

        }

    }
}
